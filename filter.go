package main

// Liefert eine neue Liste, die nur die Elemente el aus list enthält, für die pred(el) == true gilt.
func FilterByPredicate(list []int, pred func(int) bool) []int {
	result := make([]int, 0)

	for _, v := range list {
		if pred(v) {
			result = append(result, v)
		}
	}

	return result
}
