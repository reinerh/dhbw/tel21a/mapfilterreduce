package main

// Hilfsfunktion: Liefert eine Liste mit den Zahlen von 1 bis n.
func numbers(n int) []int {
	counter := 0
	return MapFunction(make([]int, n), func(int) int { counter++; return counter })
}

// Berechnet die Fakultät von n.
func Factorial(n int) int {
	return ReduceList(numbers(n), func(i, j int) int { return i * j }, 1)
}

// Berechnet die Summe der Zahlen von 1 bis n.
func SumN(n int) int {
	return ReduceList(numbers(n), func(i, j int) int { return i + j }, 0)
}

// Berechnet die Quersumme von n.
func DigitSum(n int) int {
	return ReduceList(Digits(n), func(i, j int) int { return i + j }, 0)
}

// Liefert die alternierende Summe der Zahlen in list.
// D.h. 1. Element - 2. Element + 3. Element - 4. Element + ...
func AlternatingSum(list []int) int {
	factor := -1
	return ReduceList(list, func(i, j int) int { factor *= -1; return i + factor*j }, 0)
}

// Erzeugt eine Kopie der Liste.
func Copy(list []int) []int {
	return MapFunction(list, func(i int) int { return i })
}

// Liefert eine Liste aller Elemente aus l1, die nicht in l2 enthalten sind.
func ListMinus(l1, l2 []int) []int {
	return FilterByPredicate(l1, func(i int) bool { return !Contains(l2, i) })
}

// Liefert eine Liste aller Elemente in list, die im Intervall [lower, upper] liegen.
func FilterInterval(list []int, lower, upper int) []int {
	return FilterByPredicate(list, func(i int) bool { return i >= lower && i <= upper })
}

// Liefert eine Liste, in der alle Duplikate des ersten Elements von list entfernt wurden.
func FilterDuplicates(list []int) []int {
	if len(list) == 0 {
		return make([]int, 0)
	}
	head := list[:1]
	tail := ListMinus(list[1:], head)
	return append(head, FilterDuplicates(tail)...)
}
