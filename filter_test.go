package main

import "fmt"

func ExampleFilterByPredicate() {
	l1 := []int{1, 2, 3, 4, 5}
	l2 := []int{-1, -2, 3, 2}
	l3 := []int{}
	isOdd := func(n int) bool { return n%2 != 0 }

	fmt.Println(FilterByPredicate(l1, isOdd))
	fmt.Println(FilterByPredicate(l2, isOdd))
	fmt.Println(FilterByPredicate(l3, isOdd))

	// Output:
	// [1 3 5]
	// [-1 3]
	// []
}
