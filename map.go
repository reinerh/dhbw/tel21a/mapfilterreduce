package main

// Wendet f() auf jedes Element von list an und liefert eine neue Liste mit den Ergebnissen.
func MapFunction(list []int, f func(int) int) []int {
	result := make([]int, 0)

	for _, v := range list {
		result = append(result, f(v))
	}

	return result
}
