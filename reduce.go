package main

// Wendet op() auf den Startwert und das erste Listenelement an, dann auf das Zwischenergebnis und das zweite Element etc.
// Liefert das akkumulierte Ergebnis für die gesamte Liste.
func ReduceList(list []int, op func(int, int) int, start int) int {
	result := start

	for _, v := range list {
		result = op(result, v)
	}

	return result
}
