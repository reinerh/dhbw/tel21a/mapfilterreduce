# Aufgaben zu Map, Filter und Reduce

Übungsaufgaben, bei denen Funktionen geschrieben werden sollen, mit denen auf generische Weise
Funktionen auf Listen angewendet werden sollen.

## Map

Schreiben Sie eine Funktion `MapFunction() `, die eine `int`-Slice sowie eine Funktion `f(int) int` erwartet
Die Funktion `MapFunction()` soll `f()`auf jedes einzelne Listenelement anwenden und eine neue `int`-Slice liefern,
die die Ergebnisse der Anwendung von `f()` enthält.

## Filter

Schreiben Sie eine Funktion `FilterByPredicate()`, die eine `int`-Slice sowie eine Funktion `pred(int) bool` erwartet.
Die Funktion `FilterByPredicate()` soll `pred()`auf jedes einzelne Listenelement anwenden und eine neue `int`-Slice liefern,
die nur noch diejenigen Elemente `el` enthält, für die `pred(el) == true` gilt.


*Anmerkung:* Der Funktionsname `pred` steht für "predicate" bzw. auf Deutsch "Prädikat".
             Das ist der mathematische Begriff für eine Funktion, die entscheiden soll,
             ob eine Eigenschaft für ein Objekt gilt.

## Reduce

Schreiben Sie eine Funktion `ReduceList()`, die eine `int`-Slice sowie eine Funktion `op(int,int) int`
und einen Startwert vom Typ `int` erwartet.
Die Funktion `ReduceList()` soll `op()` auf den Startwert sowie das erste Listenelement anwenden.
Das Zwischenergebnis soll dann mit dem zweiten Listen Element verrechnet werden, anschließend
mit dem dritten und so weiter.
Auf diese Weise soll die gesamte Liste "konsumiert" und ein Gesamtergebnis akkumuliert werden.

*Anmerkung:* Der Funktionsname `op` steht für "operator".
             Bekannte Beispiele für Operatoren sind die Grundrechenarten `+`, `-`, `*`, `/`, `%` etc.

## Anpassung von Funktionen

In der Datei `functions.go` sind diverse Funktionen implementiert, die verschiedene Berechnungen mit `int`-Slices durchführen.
Diese Funktionen sind mit "herkömmlichen" Schleifen geschrieben und haben dementsprechend viel mehrfachen Boilerplate-Code.

Vereinfachen Sie diese Funktionen, indem Sie so viel wie möglich die Funktionen, `MapFunction()`, `FilterByPredicate()` und/oder `ReduceList()` verwenden.

In der Datei `functions_test.go` befinden sich Tests, die weiterhin funktionieren sollten.