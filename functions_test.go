package main

import "fmt"

func ExampleFactorial() {
	fmt.Println(Factorial(3))
	fmt.Println(Factorial(5))
	fmt.Println(Factorial(0))

	// Output:
	// 6
	// 120
	// 1
}

func ExampleSumN() {
	fmt.Println(SumN(3))
	fmt.Println(SumN(5))
	fmt.Println(SumN(0))

	// Output:
	// 6
	// 15
	// 0
}

func ExampleDigitSum() {
	fmt.Println(DigitSum(3))
	fmt.Println(DigitSum(32))   // 3 + 2 == 5
	fmt.Println(DigitSum(182))  // 1 + 8 + 2 == 11
	fmt.Println(DigitSum(1802)) // 1 + 8 + 0 + 2 == 11
	fmt.Println(DigitSum(0))

	// Output:
	// 3
	// 5
	// 11
	// 11
	// 0
}

func ExampleAlternatingSum() {
	l1 := []int{1, 2, 3, 4, 5}
	l2 := []int{1, 3, 5, 7, 9}

	fmt.Println(AlternatingSum(l1)) // 1 - 2 + 3 - 4 + 5 == 3
	fmt.Println(AlternatingSum(l2)) // 1 - 3 + 5 - 7 + 9 == 5

	// Output:
	// 3
	// 5
}

func ExampleCopy() {
	l1 := []int{1, 2, 3, 4, 5}
	l1Copy := Copy(l1)
	l1Copy[2] = 42

	fmt.Println(l1)
	fmt.Println(l1Copy)

	// Output:
	// [1 2 3 4 5]
	// [1 2 42 4 5]
}

func ExampleListMinus() {
	l1 := []int{1, 2, 3, 4, 5}
	l2 := []int{1, 3, 5}

	l1Minusl2 := ListMinus(l1, l2) // Sollte nur noch 2 und 4 enthalten, weil diese in l1, aber nicht in l2 vorkommen.

	fmt.Println(l1Minusl2)

	// Output:
	// [2 4]
}

func ExampleFilterInterval() {
	l1 := []int{1, 200, 3, 4, -5}

	l2 := FilterInterval(l1, 0, 10)   // 200 und -5 sollten fehlen
	l3 := FilterInterval(l1, 10, 20)  // Leere Liste
	l4 := FilterInterval(l1, -10, 10) // 200 sollte fehlen
	l5 := FilterInterval(l1, 3, 3)

	fmt.Println(l2)
	fmt.Println(l3)
	fmt.Println(l4)
	fmt.Println(l5)

	// Output:
	// [1 3 4]
	// []
	// [1 3 4 -5]
	// [3]
}

func ExampleFilterDuplicates() {
	l1 := []int{1, 2, 3, 4, 5, 1, 2, 3}

	fmt.Println(FilterDuplicates(l1))

	// Output:
	// [1 2 3 4 5]
}
