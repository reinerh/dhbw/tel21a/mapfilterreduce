package main

import "fmt"

func ExampleMapFunction() {
	l1 := []int{1, 2, 3, 4, 5}
	l2 := []int{-1, -2, 3, 2}
	l3 := []int{}
	mal2 := func(n int) int { return n * 2 }

	fmt.Println(MapFunction(l1, mal2))
	fmt.Println(MapFunction(l2, mal2))
	fmt.Println(MapFunction(l3, mal2))

	// Output:
	// [2 4 6 8 10]
	// [-2 -4 6 4]
	// []
}
