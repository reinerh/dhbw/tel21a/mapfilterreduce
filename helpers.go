package main

/* Diese Datei enthält einige Hilfsfunktionen, die in den anderen Dateien benutzt werden (können).
   Es ist nicht Gegenstand der Aufgaben, dies Funktionen anzupassen,
   sie könnten jedoch von allgemeinem Interesse für die Vorlesung sein.
*/

// Liefert die Ziffern von n als Liste.
func Digits(n int) []int {
	if n == 0 {
		return make([]int, 0)
	}
	return append(Digits(n/10), n%10)
}

// Liefert true, falls list die Zahl n enthält.
func Contains(list []int, n int) bool {
	if len(list) == 0 {
		return false
	}
	if list[0] == n {
		return true
	}
	return Contains(list[1:], n)
}

// Liefert true, falls die Liste irgend ein Element mehrfach enthält.
func ContainsDuplicates(list []int) bool {
	return len(list) > 1 && !Contains(list[1:], list[0]) && !ContainsDuplicates(list[1:])
}
