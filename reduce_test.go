package main

import "fmt"

func ExampleReduceList() {
	l1 := []int{1, 2, 3, 4, 5}
	l2 := []int{-1, -2, 3, 2}
	l3 := []int{}
	op := func(m, n int) int { return n + m }

	fmt.Println(ReduceList(l1, op, 0))
	fmt.Println(ReduceList(l2, op, 0))
	fmt.Println(ReduceList(l3, op, 0))

	// Output:
	// 15
	// 2
	// 0
}
